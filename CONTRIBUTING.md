# Contributing

All contributions to rainboh-x are welcome.

When contributing to this project, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

## Merge Request Process

1. Fork the repository and create a topic branch from where you want to base your work (usually
   `develop`)
1. Ensure project builds and passes linting (`yarn build`)
1. Update the documentation with details of changes to the interface (new environment
   variables, exposed ports, useful file locations and container parameters)
1. Make commits of logical and atomic units. Once you'll create a commit, a git hook (installed
   by _husky_) will run _prettier_ to format the code.

## Code of Conduct

This project adheres to No Code of Conduct. We are all adults. We accept anyone's contributions.
Nothing else matters.

For more information please visit the [No Code of Conduct](https://github.com/domgetter/NCoC)
homepage.
