#!/usr/bin/env bash

# Sets files ownership with the provided UID/GID
[ -n "$MYUID" ] && usermod -u "$MYUID" $USER
[ -n "$MYGID" ] && groupmod -g "$MYGID" $USER
if [ -n "$MYUID" ] || [ -n "$MYGID" ]; then
  echo "Setting files ownership with the provided UID/GID..."
  chown $USER:$USER "$APP_DIR/rainboh-config.json"
fi

## Starts rainboh-x
echo "Starting rainboh-x..."
exec gosu $USER npm run start
