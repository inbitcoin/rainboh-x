/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import * as winston from 'winston'

const level = process.env.LOG_LEVEL || (process.env.NODE_ENV === 'production' ? 'info' : 'debug')

const allowedLevels = Object.keys(winston.config.npm.levels)
if (!allowedLevels.includes(level.toLowerCase())) {
  throw new Error('Unknown LOG_LEVEL')
}

const logger = winston.createLogger({
  level: level.toLowerCase(),
  format: winston.format.combine(winston.format.splat(), winston.format.timestamp({ format: 'HH:mm:ss' })),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.prettyPrint(),
        winston.format.colorize(),
        winston.format.printf(info => info.timestamp + ' ' + info.level + ':\t' + info.message)
      )
    })
  ]
})

logger.on('error', err => {
  // tslint:disable-next-line:no-console
  console.log('Winston logger error: ', err)
})

if (process.env.NODE_ENV !== 'production') {
  logger.debug('Logging initialized at debug level')
}

export default logger
