/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import _ from 'lodash'

import RainbohError from '../error/rainbohError'
import logger from './logger'
import ConfigProvider from './configProvider'

// tslint:disable-next-line:no-var-requires
const Client: any = require('bitcoin-core')

class RpcHelper {
  public static bitcoinCoreLogger = {
    debug: (log: any) => {
      const request = log.request
      const uriPath = request.uri.replace('http://[.+]/', '')

      if (request.type === 'response') {
        const body = JSON.stringify(request.body)
        return logger.debug(`BTC <-  %s - %o`, uriPath, body.substr(0, 30))
      }

      if (request.type === 'request') {
        const body: any = JSON.parse(request.body)

        if (body instanceof Array) {
          body.forEach(b => logger.debug(`BTC (B) ->  %s - %s - %o`, uriPath, b.method, b.params))
        } else {
          return logger.debug(`BTC ->  %s - %s - %o`, uriPath, body.method, body.params)
        }
      }

      return logger.debug(`BTC <->  %j`, log)
    }
  }

  public static async checkConnection() {
    const config = ConfigProvider.getInstance().getConfig()

    const coreClient = new Client({
      version: config.bitcoind.version,
      network: config.network,
      host: config.bitcoind.host,
      port: config.bitcoind.rpcPort,
      username: config.bitcoind.rpcUsername,
      password: config.bitcoind.rpcPassword,
      timeout: config.bitcoind.timeout
    })

    const networkInfo = await coreClient.getNetworkInfo()
    const blockchainInfo = await coreClient.getBlockchainInfo()

    if (!ConfigProvider.SUPPORTED_BITCOIND_VERSIONS.some(v => String(networkInfo.version).startsWith(v)))
      throw new RainbohError(RainbohError.BITCOIND_VERSION_NOT_SUPPORTED)

    if (!config.network.startsWith(blockchainInfo.chain)) throw new RainbohError(RainbohError.BITCOIND_NETWORK_MISMATCH)

    logger.info(`Connected to Bitcoind ${networkInfo.subversion} @ ${config.bitcoind.host}:${config.bitcoind.rpcPort}`)
    logger.info(`Network: ${config.network}`)
    return true
  }

  public static isJsonKeyDuplicate(jsonString: string) {
    const regex = /[\"\']([A-Za-z0-9]+)[\"\']/g
    const keys = ['"amount"', '"amountBtc"']
    const addresses = jsonString.match(regex)?.filter(e => keys.indexOf(e) < 0)

    const addr = addresses ? addresses.map(e => e.substr(1, e.length - 2)) : []
    return _.uniq(addr).length !== addr.length
  }

  public static getErrorObj(err: any) {
    if (err instanceof RainbohError) return err.toObject()
    if (err.name === 'RpcError') return err

    return {
      code: 500,
      name: 'Internal Error',
      message: err.message
    }
  }

  public static logError(err: any) {
    if (process.env.NODE_ENV === 'production') logger.error('ERR -- %o', err.message)
    else logger.error('ERR -- %o\n\t%s', err.message, err.stack || '')
  }
}

export default RpcHelper
