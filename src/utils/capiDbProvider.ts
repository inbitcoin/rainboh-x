/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Db, MongoClient } from 'mongodb'

import ConfigProvider from './configProvider'
import logger from './logger'
import RainbohError from '../error/rainbohError'

class CapiDbProvider {
  public static getInstance() {
    if (!CapiDbProvider._instance) {
      CapiDbProvider._instance = new CapiDbProvider()
    }
    return CapiDbProvider._instance
  }

  private static _instance: CapiDbProvider
  private static DEFAULT_DB_NAME = 'explorer_production'
  private static COLLECTION_RAW_TRANSACTIONS = 'rawtransactions'

  private readonly client: MongoClient
  private readonly dbName: string
  private readonly url: string
  private db?: Db

  private constructor() {
    const config = ConfigProvider.getInstance().getConfig()

    this.url = `mongodb://${config.capi.db.host}:${config.capi.db.port}/`

    this.dbName = config.capi.db.name || CapiDbProvider.DEFAULT_DB_NAME
    this.client = new MongoClient(this.url, { useNewUrlParser: true, useUnifiedTopology: true })
  }

  public async connect() {
    logger.info(`Connecting to Capi DB: ${this.url}${this.dbName}`)

    await this.client.connect()
    this.db = this.client.db(this.dbName)

    const collections = (await this.db.collections()).map(c => c.collectionName)
    if (!collections.includes(CapiDbProvider.COLLECTION_RAW_TRANSACTIONS))
      throw new RainbohError(RainbohError.DB_COLLECTION_NOT_FOUND(CapiDbProvider.COLLECTION_RAW_TRANSACTIONS))

    logger.info(`Connected to Capi DB: ${this.url}${this.dbName}`)
  }

  public async getTransaction(txid: string) {
    if (!this.db) {
      logger.error('Error getting data from DB')
      process.exit(1)
    }
    const collection = this.db.collection(CapiDbProvider.COLLECTION_RAW_TRANSACTIONS)

    return await collection.findOne({ txid: txid })
  }
}

export default CapiDbProvider
