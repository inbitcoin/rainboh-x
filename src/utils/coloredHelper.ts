/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import _ from 'lodash'
import * as BitcoinLib from 'bitcoinjs-lib'
import ColoredCoinsBuilder from '@inbitcoin/ctxbuilder'

import ConfigProvider from './configProvider'
import { IUtxo as CapiUtxo } from '../interfaces/capi'
import { IAmountColBtc } from '../interfaces/wallet'
import Wallet from '../lib/wallet'
import WalletCol from '../lib/walletCol'
import WalletBtc from '../lib/walletBtc'
import { IColoredConfig } from '../interfaces/rainbohConfig'
import BitcoinHelper from './bitcoinHelper'
import logger from './logger'

class ColoredHelper {
  public static MIN_DUST_FEE = 546

  public static getColoredCoinsBuilder(addressHrp?: string): ColoredCoinsBuilder {
    const config = ConfigProvider.getInstance()
    return new ColoredCoinsBuilder({
      network: config.getNetwork(),
      minDustValue: ColoredHelper.MIN_DUST_FEE,
      softMaxUtxos: config.getConfig().wallets.coloredOptions.softMaxUtxos,
      assetAddressHrp: addressHrp
    })
  }

  public static isColBech32Address(address: string, hrp: string): boolean {
    const config = ConfigProvider.getInstance()
    if (config.getNetworkCapi() === 'mainnet') {
      return address.startsWith(hrp)
    } else {
      return address.startsWith('t' + hrp)
    }
  }

  public static toColAddress(addressBtc: string, coloredConfig: IColoredConfig): string {
    // already colored address
    if (
      addressBtc[0] === coloredConfig.addressPrefix ||
      this.isColBech32Address(addressBtc, coloredConfig.addressHrp)
    ) {
      return addressBtc
    }

    // try bech32 address
    const config = ConfigProvider.getInstance()
    const coloredCoinsBuilder = new ColoredCoinsBuilder({
      network: config.getNetwork(),
      assetAddressHrp: coloredConfig.addressHrp
    })
    try {
      return coloredCoinsBuilder.toAssetBech32Address(addressBtc)
    } catch (exp) {
      // legacy
      if (BitcoinHelper.isValidAddress(addressBtc)) {
        return coloredConfig.addressPrefix + addressBtc
      }
    }
    throw new Error(`${addressBtc} is not a valid address`)
  }

  public static toBtcAddress(addressCol: string, coloredConfig: IColoredConfig): string {
    // legacy
    if (addressCol[0] === coloredConfig.addressPrefix) {
      return addressCol.slice(1)
    }
    // bech32
    if (this.isColBech32Address(addressCol, coloredConfig.addressHrp)) {
      const config = ConfigProvider.getInstance()
      const coloredCoinsBuilder = new ColoredCoinsBuilder({
        network: config.getNetwork(),
        assetAddressHrp: coloredConfig.addressHrp
      })
      return coloredCoinsBuilder.toBitcoinBech32Address(addressCol)
    }

    // we received a bitcoin address
    return addressCol
  }

  public static toUnit(amount: number, divisibility: number): number {
    return _.round(amount * 10 ** -divisibility, divisibility)
  }
  public static toEwl(amount: number, divisibility: number): number {
    return _.round(amount * 10 ** divisibility)
  }
  // TODO: mv toSat and toBtc to bitcoinHelper
  public static toSat(amount: number): number {
    return _.round(amount * 10 ** 8)
  }
  public static toBtc(amount: number): number {
    return _.round(amount * 10 ** -8, 8)
  }

  public static toAmountColBtc(value: string | number | IAmountColBtc): IAmountColBtc {
    if (typeof (value as IAmountColBtc).amount === 'number') return value as IAmountColBtc
    // the string "" or " " are converted as 0, so we set amount=-1
    // to raise an error on transaction creation.
    if (Number(value) === 0) return { amount: -1 }
    else return { amount: Number(value) }
  }

  public static isValidAddress(address: string, coloredConfig: IColoredConfig): boolean {
    const network = ConfigProvider.getInstance().getNetworkBitcoinLib()
    const coloredCoinsBuilder = this.getColoredCoinsBuilder(coloredConfig.addressHrp)

    if (typeof address !== 'string') return false
    if (address[0] === coloredConfig.addressPrefix) {
      // legacy
      const btcAddress = address.substr(1)

      try {
        BitcoinLib.address.toOutputScript(btcAddress, network)
        return true
      } catch (e) {
        return false
      }
    } else {
      // bech32
      return coloredCoinsBuilder.isValidAssetBech32Address(address)
    }
  }

  public static async createTx(
    capiUtxos: CapiUtxo[],
    to: Array<{ address: string; amount: number; amountBtc?: number }>,
    feeRateKB: number = 1000,
    opts: { walletBtc: WalletBtc; walletCol: WalletCol }
  ) {
    if (!opts.walletBtc || !opts.walletCol) throw new Error('Missing Params')

    const coloredConfig = opts.walletCol.getColoredConfig()

    const coloredCoinsBuilder = this.getColoredCoinsBuilder(coloredConfig.addressHrp)

    const toArg = to.map(t => {
      return {
        address: ColoredHelper.toBtcAddress(t.address, coloredConfig),
        amount: t.amount,
        amountBtc: t.amountBtc,
        assetId: coloredConfig.assetId
      }
    })

    const builderArgs: IBuilderArgsSend = {
      utxos: capiUtxos,
      to: toArg,
      feePerKb: feeRateKB,
      changeAddress: async () => this.toBtcAddress(await opts.walletCol.getRawChangeAddress(), coloredConfig),
      bitcoinChangeAddress: async () => await opts.walletBtc.getRawChangeAddress()
    }

    const tx = await coloredCoinsBuilder.buildSendTransaction(builderArgs)
    if (!tx) {
      throw Error('coloredCoinsBuilder.buildSendTransaction is broken -> ' + tx)
    }
    return tx.txHex
  }

  /* createRawColTx (vs createColTx)
   * - fee is not computed
   * - changes outputs are not created
   * - all the inputs will be used
   */
  public static async createRawColTx(
    capiUtxos: CapiUtxo[],
    to: Array<{ address: string; amount: number; amountBtc?: number }>,
    opts: { walletBtc: WalletBtc; walletCol: WalletCol }
  ): Promise<string> {
    if (!opts.walletBtc || !opts.walletCol) throw new Error('Missing Params')

    const coloredConfig = opts.walletCol.getColoredConfig()

    const coloredCoinsBuilder = this.getColoredCoinsBuilder(coloredConfig.addressHrp)

    const toArg = to.map(t => {
      return {
        address: ColoredHelper.toBtcAddress(t.address, coloredConfig),
        amount: t.amount,
        amountBtc: t.amountBtc,
        assetId: coloredConfig.assetId
      }
    })

    const builderArgs: IBuilderArgsSend = {
      utxos: capiUtxos,
      to: toArg,
      rawMode: true
    }

    // Copy builderArgs because the library modify its value
    const tx = await coloredCoinsBuilder.buildSendTransaction({ ...builderArgs })
    if (!tx) {
      throw Error('coloredCoinsBuilder.buildSendTransaction is broken -> ' + tx)
    }
    return tx.txHex
  }

  public static async createIssueTx(
    capiUtxos: CapiUtxo[],
    to: Array<{ address: string; amount: number }>,
    divisibility: number,
    feeRateKB: number = 1000,
    opts: { walletBtc: WalletBtc }
  ) {
    if (!opts.walletBtc) throw new Error('Missing Params')

    const coloredCoinsBuilder = this.getColoredCoinsBuilder()

    const toArg = to.map(t => {
      return {
        address: t.address,
        amount: t.amount
      }
    })

    const changeAddressBtc = await opts.walletBtc.getRawChangeAddress()

    const builderArgs = {
      utxos: capiUtxos,
      fee: 1,
      issueAddress: changeAddressBtc,
      amount: _.sumBy(toArg, 'amount'),
      divisibility: divisibility,
      transfer: toArg
    }

    let coloredTx = await coloredCoinsBuilder.buildIssueTransaction(builderArgs)

    do {
      builderArgs.fee = ColoredHelper.estimateFee(coloredTx.txHex, feeRateKB)
      coloredTx = await coloredCoinsBuilder.buildIssueTransaction(builderArgs)
    } while (builderArgs.fee < ColoredHelper.estimateFee(coloredTx.txHex, feeRateKB))

    return {
      hex: coloredTx.txHex,
      assetId: coloredTx.assetId
    }
  }

  public static async signTx(hex: string, walletCol: WalletCol, walletBtc: WalletBtc): Promise<string> {
    let psbt = await new Wallet().convertToPsbt(hex)

    const processedPsbtCol = await walletCol.signPsbt(psbt)
    psbt = processedPsbtCol.psbt

    if (!processedPsbtCol.complete) {
      const processedPsbtBtc = await walletBtc.signPsbt(psbt)
      psbt = processedPsbtBtc.psbt

      if (!processedPsbtBtc.complete) throw new Error('Transaction not completed')
    }

    return (await new Wallet().finalizePsbt(psbt)).hex
  }

  public static estimateFee(txHex: string, feeRateKB: number): number {
    const SIGNATURE_SIZE = 110
    const txSizeBytes = txHex.length / 2
    const inputsCount = BitcoinLib.Transaction.fromHex(txHex).ins.length

    return Math.round((txSizeBytes + inputsCount * SIGNATURE_SIZE) * (feeRateKB / 1000))
  }
}

export default ColoredHelper
