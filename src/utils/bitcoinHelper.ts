/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import * as BitcoinLib from 'bitcoinjs-lib'

import ConfigProvider from './configProvider'
import WalletBtc from '../lib/walletBtc'
import Wallet from '../lib/wallet'

class BitcoinHelper {
  public static isValidAddress(address: string) {
    const network = ConfigProvider.getInstance().getNetworkBitcoinLib()

    // BitcoinLib supports also bech32 addresses
    try {
      BitcoinLib.address.toOutputScript(address, network)
      return true
    } catch (e) {
      return false
    }
  }

  public static async signTx(hex: string, walletBtc: WalletBtc): Promise<string> {
    let psbt = await new Wallet().convertToPsbt(hex)

    const processedPsbtBtc = await walletBtc.signPsbt(psbt)
    psbt = processedPsbtBtc.psbt

    if (!processedPsbtBtc.complete) throw new Error('Transaction not completed')

    return (await new Wallet().finalizePsbt(psbt)).hex
  }
}

export default BitcoinHelper
