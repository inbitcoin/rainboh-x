/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import express from 'express'
import * as jayson from 'jayson'

import * as routesRpc from './routes/rpc'
import * as routesRpcTest from './routes/rpcTest'
import ConfigProvider from './utils/configProvider'
import logger from './utils/logger'
import RainbohError from './error/rainbohError'

const DEFAULT_PORT = 7872

// Create Express server
const app = express()

// Express configuration
app.set('port', process.env.PORT || DEFAULT_PORT)

app.use(express.json())

app.use((req, res, next) => {
  const method = req.body.method
  const params = req.body.params
  const paramsLog = [...params]
  if (method === 'encryptwallet' || method === 'walletpassphrase') paramsLog[0] = '******'
  if (method === 'walletpassphrasechange') paramsLog[0] = paramsLog[1] = '******'

  logger.info(`${req.method} -> %s - %s - %o`, req.path, req.body.method, paramsLog)
  return next()
})

// Catch Json parsing errors
app.use((err: any, req: any, res: any, next: any) => {
  // @ts-ignore
  if (err instanceof SyntaxError && err.status >= 400 && err.status < 500 && err.message.indexOf('JSON')) {
    res.status(400)
    return res.json({
      msg: 'Malformed request'
    })
  }
  return next()
})

app.all('/wallet/:walletName', (req, res, next) => {
  const walletName = req.params.walletName
  const configProvider = ConfigProvider.getInstance()

  if (configProvider.isValidWalletName(walletName)) {
    res.locals.walletName = walletName
    res.locals.walletType = ConfigProvider.getInstance().getWalletType(walletName)
  } else {
    res.status(500)
    return res.send({
      id: req.body.id,
      error: new RainbohError(RainbohError.INVALID_WALLET).toObject()
    })
  }

  return next()
})

/**
 * Primary app routes.
 */
const routes = app.get('env') === 'test' ? { ...routesRpcTest, ...routesRpc } : routesRpc

// @ts-ignore
const server = jayson.server(routes, { version: 1, useContext: true, params: Array })

app.use((req, res) => {
  const context = res.locals

  server.call(req.body, context, (err: any, result: any) => {
    if (err) {
      res.status(500)
      res.send(err)
    }
    return res.send(result)
  })
})

export default app
