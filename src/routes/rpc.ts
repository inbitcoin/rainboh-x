/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import RainbohError from '../error/rainbohError'
import Wallet from '../lib/wallet'
import WalletCol from '../lib/walletCol'
import WalletBtc from '../lib/walletBtc'
import ConfigProvider from '../utils/configProvider'
import RpcHelper from '../utils/rpcHelper'
import { EstimateMode } from '../interfaces/wallet'
import { ICreatePsbtInputs } from '../interfaces/wallet'

/**
 * Creates and loads a new wallet
 */
export async function createwallet(args: any, context: any, done: (err: any, res?: any) => void) {
  const walletName: string = args[0] || undefined

  if (args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!ConfigProvider.getInstance().isValidWalletName(walletName)) {
    const err = new RainbohError(RainbohError.INVALID_WALLET).toObject()
    RpcHelper.logError(err)
    return done(err)
  }

  try {
    const res = await new Wallet().createWallet(walletName)

    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Loads wallet
 */
export async function loadwallet(args: any, context: any, done: (err: any, res?: any) => void) {
  const walletName: string = args[0]

  if (!walletName || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!ConfigProvider.getInstance().isValidWalletName(walletName)) {
    const err = new RainbohError(RainbohError.INVALID_WALLET).toObject()
    RpcHelper.logError(err)
    return done(err)
  }

  try {
    const res = await new Wallet().loadWallet(walletName)
    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Unload wallet
 */
export async function unloadwallet(args: any, context: any, done: (err: any, res?: any) => void) {
  const walletName: string = args[0] || undefined

  if (args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!ConfigProvider.getInstance().isValidWalletName(walletName)) {
    const err = new RainbohError(RainbohError.INVALID_WALLET).toObject()
    RpcHelper.logError(err)
    return done(err)
  }

  try {
    const res = await new Wallet().unloadWallet(walletName)
    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Returns a list of currently loaded wallets
 */
export async function listwallets(args: any, context: any, done: (err: any, res?: any) => void) {
  if (args[0]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  try {
    const res = await new Wallet().listWallets()
    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Returns a new address for receiving payments
 */
export async function getnewaddress(args: any, context: any, done: (err: any, res?: any) => void) {
  if (args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const address = await wallet.getNewAddress(args[1])
    return done(null, address)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Returns the total available balance
 */
export async function getbalance(args: any, context: any, done: (err: any, res?: any) => void) {
  const dummy: string = args[0] || ''
  const minConf: number = args[1] ? Number(args[1]) : 0

  if (isNaN(minConf)) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const amount = await wallet.getBalance(minConf)
    return done(null, amount)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Returns array of unspent transaction outputs
 */
export async function listunspent(args: any, context: any, done: (err: any, res?: any) => void) {
  const minConf: number = args[0] || undefined
  const maxConf: number = args[1] || undefined

  if (args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const listUnspent = await wallet.listUnspent(minConf, maxConf)
    return done(null, listUnspent)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Returns hash of block in best-block-chain at height provided
 */
export async function getblockhash(args: any, context: any, done: (err: any, res?: any) => void) {
  const height = args[0]

  if (!Number.isInteger(height) || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  try {
    const hash = await new Wallet().getBlockHash(height)
    return done(null, hash)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 *  Returns the number of blocks in the longest block chain
 */
export async function getblockcount(args: any, context: any, done: (err: any, res?: any) => void) {
  if (args[0]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  try {
    const height = await new Wallet().getBlockCount()
    done(null, height)
  } catch (err) {
    RpcHelper.logError(err)
    done(null, err.message)
  }
}

/**
 * Returns information about the block with the given hash
 */
export async function getblock(args: any, context: any, done: (err: any, res?: any) => void) {
  const blockHash = args[0]
  const verbosity = args[1]

  if (!blockHash || args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  try {
    const block = await new Wallet().getBlock(blockHash, verbosity)
    return done(null, block)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Send an amount to a given address
 */
export async function sendtoaddress(args: any, context: any, done: (err: any, res?: any) => void) {
  const address = args[0]
  const amount = args[1]
  const confTarget: number = args[6]

  if (args[2] !== '' && args[2] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (args[3] !== '' && args[3] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (args[4] !== '' && args[4] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (args[5] !== '' && args[5] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (!address || !amount || args.length > 7) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const txid = await wallet.sendToAddress(address, amount, confTarget)
    return done(null, txid)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Send to multiple address
 */
export async function sendmany(args: any, context: any, done: (err: any, res?: any) => void) {
  const amounts = args[1]
  const confTarget: number = args[6]
  const estimateMode: EstimateMode = args[7]

  if (args[0] !== '') return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (args[2] !== '' && args[2] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (args[3] !== '' && args[3] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (args[4] !== '' && args[4] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (args[5] !== '' && args[5] !== undefined) return done(new RainbohError(RainbohError.INVALID_DUMMY).toObject())
  if (!args[1] || args.length > 8) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  const estimateModeValues = ['UNSET', 'ECONOMICAL', 'CONSERVATIVE']
  if (estimateMode && !estimateModeValues.includes(estimateMode))
    return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const txid = await wallet.sendMany(amounts, confTarget, estimateMode)
    return done(null, txid)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Stores the wallet decryption key in Bitcoin Core memory for <timeout> seconds
 */
export async function walletpassphrase(args: any, context: any, done: (err: any, res?: any) => void) {
  const passphrase = args[0]
  const timeout = args[1]

  if (!passphrase || !timeout || args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const res = await wallet.walletPassphrase(passphrase, timeout)

    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Changes the wallet passphrase from <oldpassphrase> to <newpassphrase>
 */
export async function walletpassphrasechange(args: any, context: any, done: (err: any, res?: any) => void) {
  const oldPassphrase = args[0]
  const newPassphrase = args[1]

  if (args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const res = await wallet.walletPassphraseChange(oldPassphrase, newPassphrase)

    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Removes the wallet encryption key from Bitcoin Core memory, locking the wallet
 */
export async function walletlock(args: any, context: any, done: (err: any, res?: any) => void) {
  if (args[0]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const res = await wallet.walletLock()

    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Encrypts the wallet with <passphrase>. This is for first time encryption
 */
export async function encryptwallet(args: any, context: any, done: (err: any, res?: any) => void) {
  const passphrase = args[0]

  if (args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const res = await wallet.encryptWallet(passphrase)

    return done(null, res)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 *  Returns data about each connected network node as a json array of objects
 */
export async function getpeerinfo(args: any, context: any, done: (err: any, res?: any) => void) {
  if (args[0]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  try {
    const peerInfo = await new Wallet().getPeerInfo()

    return done(null, peerInfo)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Returns up to <count> most recent transactions skipping the first <skip> transactions
 */
export async function listtransactions(args: any, context: any, done: (err: any, res?: any) => void) {
  const label = '*'
  const count = args[1]
  const skip = args[2]

  if (args[3]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const listTransaction = await wallet.listTransactions(label, count, skip)

    return done(null, listTransaction)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 *  Returns the number of connections to other nodes
 */
export async function getconnectioncount(args: any, context: any, done: (err: any, res?: any) => void) {
  if (args[0]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  try {
    const connections = await new Wallet().getConnectionCount()

    return done(null, connections)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Get detailed information about in-wallet transaction <txid>
 */
export async function gettransaction(args: any, context: any, done: (err: any, res?: any) => void) {
  const txid = args[0]

  if (!txid || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const tx = await wallet.getTransaction(txid)

    return done(null, tx)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Returns the currently loaded config
 */
export function getconfig(args: any, context: any, done: (err: any, res?: any) => void) {
  if (args[0]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  const config = ConfigProvider.getInstance().getSafeConfig()

  done(null, config)
}

/**
 * Returns the number of amounts that can be fitted into a transactions
 */
export async function opreturnlimit(args: any, context: any, done: (err: any, res?: any) => number) {
  const amounts = args[0]
  if (!amounts || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  if (context.walletType !== ConfigProvider.WALLET_TYPE_COLORED)
    return done(new RainbohError(RainbohError.WALLET_COLORED_ONLY).toObject())

  const wallet = new WalletCol(context.walletName)

  try {
    const n = await wallet.opReturnLimit(amounts)
    return done(null, n)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Return the raw transaction data.
 * verbose=true is not supported on a colored wallet
 */
export async function getrawtransaction(args: any, context: any, done: (err: any, res?: any) => void) {
  const txid = args[0]
  const verbose = args[1] || false
  const blockhash = args[2] || null

  if (!txid || args[3]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  // Bitcoin Core does not require the wallet for this call, but we use the wallet to
  // select the output format:
  // - WALLET_TYPE_COLORED: amount is colored and amountBtc is defined
  // - WALLET_TYPE_BITCOIN: amount for bitcoins and amountBtc is not defined
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())
  if (verbose && context.walletType === ConfigProvider.WALLET_TYPE_COLORED)
    return done(new RainbohError(RainbohError.WALLET_BTC_ONLY).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const tx = await wallet.getRawTransaction(txid, verbose, blockhash)

    return done(null, tx)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Return a JSON object representing the serialized, hex-encoded transaction.
 */
export async function decoderawtransaction(args: any, context: any, done: (err: any, res?: any) => void) {
  const hex = args[0]
  const isWitness = args[1] || null

  if (!hex || args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())
  if (context.walletType === ConfigProvider.WALLET_TYPE_COLORED)
    return done(new RainbohError(RainbohError.WALLET_BTC_ONLY).toObject())

  const wallet = new WalletBtc(context.walletName)

  try {
    const tx = await wallet.decodeRawTransaction(hex, isWitness)

    return done(null, tx)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Return a JSON object representing the serialized, hex-encoded transaction.
 */
export async function sendrawtransaction(args: any, context: any, done: (err: any, res?: any) => void) {
  const hex = args[0]

  if (!hex || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  const wallet = new Wallet()

  try {
    const tx = await wallet.sendRawTransaction(hex)

    return done(null, tx)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Update a PSBT with input information from our wallet and then sign inputs
 * that we can sign for.
 */
export async function walletprocesspsbt(args: any, context: any, done: (err: any, res?: any) => void) {
  const psbt = args[0]

  if (!psbt || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())

  const wallet =
    context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
      ? new WalletBtc(context.walletName)
      : new WalletCol(context.walletName)

  try {
    const tx = await wallet.signPsbt(psbt)

    return done(null, tx)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Creates a transaction in the Partially Signed Transaction format.
 * Implements the Creator role
 */
export async function createpsbt(args: any, context: any, done: (err: any, res?: any) => void) {
  const inputs: ICreatePsbtInputs = args[0]
  const outputs = args[1]

  if (!inputs || !outputs || args[2]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())
  try {
    const wallet =
      context.walletType === ConfigProvider.WALLET_TYPE_BITCOIN
        ? new WalletBtc(context.walletName)
        : new WalletCol(context.walletName)

    const psbt = await wallet.createPsbt(inputs, outputs)

    return done(null, psbt)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Finalize the inputs of a PSBT. If the transaction is fully signed, it will produce a
 * network serialized transaction which can be broadcast with sendrawtransaction
 */
export async function finalizepsbt(args: any, context: any, done: (err: any, res?: any) => void) {
  const psbt = args[0]

  if (!psbt || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  const wallet = new Wallet()

  try {
    const tx = await wallet.finalizePsbt(psbt)

    return done(null, tx)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Estimates the approximate fee per kilobyte needed for a transaction to begin
 * confirmation within conf_target blocks if possible and return the number of blocks
 * for which the estimate is valid
 */
export async function estimatesmartfee(args: any, context: any, done: (err: any, res?: any) => void) {
  const confTarget = args[0]

  if (!confTarget || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  const wallet = new Wallet()

  try {
    const tx = await wallet.estimateSmartFee(confTarget)

    return done(null, tx)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}
