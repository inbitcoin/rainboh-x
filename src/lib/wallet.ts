/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// tslint:disable-next-line:no-var-requires
const BitcoinClient: any = require('bitcoin-core')

import ConfigProvider from '../utils/configProvider'
import RpcHelper from '../utils/rpcHelper'
import { EstimateMode } from '../interfaces/wallet'

class Wallet {
  public static WALLET_DIR = `${__dirname}/../../wallets`

  protected readonly coreClient: any
  protected readonly configProvider: ConfigProvider

  constructor(walletName?: string) {
    this.configProvider = ConfigProvider.getInstance()
    const config = this.configProvider.getConfig()

    const coreClientOpts = {
      version: config.bitcoind.version,
      network: config.network,
      host: config.bitcoind.host,
      port: config.bitcoind.rpcPort,
      username: config.bitcoind.rpcUsername,
      password: config.bitcoind.rpcPassword,
      wallet: walletName,
      logger: RpcHelper.bitcoinCoreLogger
    }

    this.coreClient = new BitcoinClient(coreClientOpts)
  }

  public createWallet(walletName: string) {
    return this.coreClient.createWallet(walletName)
  }

  public loadWallet(walletName: string) {
    return this.coreClient.loadWallet(walletName)
  }

  public unloadWallet(walletName: string) {
    return this.coreClient.unloadWallet(walletName)
  }

  public listWallets() {
    return this.coreClient.listWallets()
  }

  public getBlockCount(): Promise<string> {
    return this.coreClient.getBlockCount()
  }

  public getBlockHash(height: number): Promise<any> {
    return this.coreClient.getBlockHash(height)
  }

  public getBlock(hash: string, verbosity?: number): Promise<any> {
    return this.coreClient.getBlock(hash, verbosity)
  }

  public getPeerInfo(): Promise<any> {
    return this.coreClient.getPeerInfo()
  }

  public getConnectionCount(): Promise<any> {
    return this.coreClient.getConnectionCount()
  }

  public walletPassphrase(passphrase: string, timeout: number): Promise<any> {
    return this.coreClient.walletPassphrase(passphrase, timeout)
  }

  public walletLock(): Promise<any> {
    return this.coreClient.walletLock()
  }

  public walletPassphraseChange(oldPassphrase: string, newPassphrase: string): Promise<any> {
    return this.coreClient.walletPassphraseChange(oldPassphrase, newPassphrase)
  }

  public encryptWallet(passphrase: string): Promise<any> {
    return this.coreClient.encryptWallet(passphrase)
  }

  public convertToPsbt(hex: string): Promise<string> {
    return this.coreClient.convertToPsbt(hex)
  }

  public signPsbt(psbt: string) {
    return this.coreClient.walletProcessPsbt(psbt)
  }

  public finalizePsbt(psbt: string) {
    return this.coreClient.finalizePsbt(psbt)
  }

  public sendRawTransaction(hex: string) {
    return this.coreClient.sendRawTransaction(hex)
  }

  public getRawTransaction(txid: string, verbose: boolean = false, blockhash?: string): Promise<string> {
    return this.coreClient.getRawTransaction(txid, verbose, blockhash)
  }

  public decodeRawTransaction(hex: string, isWitness?: boolean): Promise<any> {
    if (typeof isWitness === 'boolean') return this.coreClient.decodeRawTransaction(hex, isWitness)
    else return this.coreClient.decodeRawTransaction(hex)
  }

  public decodePsbt(psbt: string) {
    return this.coreClient.decodePsbt(psbt)
  }

  public getWalletInfo() {
    return this.coreClient.getWalletInfo()
  }

  public estimateSmartFee(confTarget: number, estimateMode?: EstimateMode) {
    if (estimateMode) return this.coreClient.estimateSmartFee(confTarget, estimateMode)
    else return this.coreClient.estimateSmartFee(confTarget)
  }
}

export default Wallet
