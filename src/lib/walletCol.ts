/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import _ from 'lodash'

import Wallet from './wallet'
import ColoredHelper from '../utils/coloredHelper'
import { IUtxo as CapiUtxo } from '../interfaces/capi'
import { ICreatePsbtInputs, IColCreatePsbtOutputs, IColSendManyOutputs, EstimateMode } from '../interfaces/wallet'
import { IColoredConfig, IWalletBtcConfig, IWalletColConfig } from '../interfaces/rainbohConfig'
import CapiDbProvider from '../utils/capiDbProvider'
import WalletBtc from './walletBtc'
import RainbohError from '../error/rainbohError'
import logger from '../utils/logger'
import ColoredCoinsBuilder from '@inbitcoin/ctxbuilder'

class WalletCol extends Wallet {
  private readonly capiDbProvider: CapiDbProvider
  private readonly walletConfig: IWalletColConfig
  private readonly walletBtcConfig: IWalletBtcConfig
  private readonly coloredConfig: IColoredConfig

  constructor(walletName: string) {
    super(walletName)

    this.capiDbProvider = CapiDbProvider.getInstance()
    this.walletConfig = this.configProvider.getWalletColConfig(walletName)
    this.walletBtcConfig = this.configProvider.getWalletBtcConfig(this.walletConfig.btcWallet)
    this.coloredConfig = this.configProvider.getColoredConfig(this.walletConfig.coin)
  }

  public async getNewAddress(addressType?: 'legacy' | 'bech32'): Promise<string> {
    const label = ''

    if (!addressType) addressType = 'legacy'

    if (addressType !== 'legacy' && addressType !== 'bech32') throw new RainbohError(RainbohError.INVALID_ADDRESS_TYPE)

    const address = await this.coreClient.getNewAddress(label, addressType)

    return ColoredHelper.toColAddress(address, this.coloredConfig)
  }

  public async getRawChangeAddress(): Promise<string> {
    const addressType = 'bech32'

    const address = await this.coreClient.getRawChangeAddress(addressType)

    return ColoredHelper.toColAddress(address, this.coloredConfig)
  }

  public async listUnspent(minConf: number = 0, maxConf: number = 9999999): Promise<any> {
    const listUnspent = await this.coreClient.listUnspent(minConf, maxConf)

    for (const unspent of listUnspent) {
      const capiTx = await this.capiDbProvider.getTransaction(unspent.txid)

      const assetInfo = this.getUtxoAssetInfo(capiTx, unspent.vout)

      if (assetInfo) {
        unspent.amountBtc = unspent.amount
        unspent.amount = ColoredHelper.toUnit(assetInfo.amount, assetInfo.divisibility)
      } else {
        unspent.amountBtc = unspent.amount
        unspent.amount = 0
      }

      if (unspent.address) unspent.address = ColoredHelper.toColAddress(unspent.address, this.coloredConfig)
    }

    return listUnspent
  }

  public async getBalance(minConf: number = 1): Promise<number> {
    const listUnspent = await this.listUnspent(minConf)
    return _.round(_.sumBy(listUnspent, 'amount'), this.coloredConfig.divisibility)
  }

  public async listTransactions(label: string, count: number, skip: number) {
    const listTransaction = await this.coreClient.listTransactions(label, count, skip)

    for (const tx of listTransaction) {
      const capiTx = await this.capiDbProvider.getTransaction(tx.txid)

      const assetInfo = this.getUtxoAssetInfo(capiTx, tx.vout)

      if (assetInfo) {
        tx.amountBtc = tx.amount
        tx.amount = ColoredHelper.toUnit(assetInfo.amount, assetInfo.divisibility)
        tx.amount = tx.category === 'send' ? -tx.amount : tx.amount
      } else {
        tx.amountBtc = tx.amount
        tx.amount = 0
      }

      if (tx.address) tx.address = ColoredHelper.toColAddress(tx.address, this.coloredConfig)
    }

    return listTransaction
  }

  public async getTransaction(txid: string) {
    const tx = await this.coreClient.getTransaction(txid)

    const capiTx = await this.capiDbProvider.getTransaction(tx.txid)

    tx.amountBtc = tx.amount
    tx.amount = 0

    for (const detail of tx.details) {
      const assetInfo = this.getUtxoAssetInfo(capiTx, detail.vout, detail.address)

      if (assetInfo) {
        detail.amountBtc = detail.amount
        detail.amount = ColoredHelper.toUnit(assetInfo.amount, assetInfo.divisibility)
        detail.amount = detail.category === 'send' ? -detail.amount : detail.amount

        tx.amount = _.round(tx.amount + detail.amount, assetInfo.divisibility)
      } else {
        detail.amountBtc = detail.amount
        detail.amount = 0
      }

      if (detail.address) detail.address = ColoredHelper.toColAddress(detail.address, this.coloredConfig)
    }

    return tx
  }

  public async sendToAddress(address: string, amount: number, confTarget?: number) {
    if (!ColoredHelper.isValidAddress(address, this.coloredConfig)) throw new RainbohError(RainbohError.INVALID_ADDRESS)
    if (amount < 1 / 10 ** this.coloredConfig.divisibility) throw new RainbohError(RainbohError.INVALID_AMOUNT)

    const walletBtc = new WalletBtc(this.walletBtcConfig.name)
    const amountEwl = ColoredHelper.toEwl(amount, this.coloredConfig.divisibility)

    const capiUtxoCol = await this.getCapiUtxos(1)
    const capiUtxoBtc = await walletBtc.getCapiUtxos(1)

    // estimateSmartFee responses with errors if bitcoind doesn't have enough information to calculate fees
    const feeEstimation = await this.estimateSmartFee(confTarget || 3)
    // if (feeEstimation.errors) throw new RainbohError(RainbohError.FEE_ESTIMATION_NOT_AVAILABLE)
    if (!feeEstimation.feerate) logger.warn('Fee estimation not available. Fee set to 1000 sat/KB')
    const feeRate = feeEstimation.feerate || 0.00001 // fallback to 1 sat/B

    const feeSatPerKB = ColoredHelper.toSat(feeRate)

    logger.debug('Choose fee: %s sat/KB', feeSatPerKB)

    try {
      let hex = await ColoredHelper.createTx(
        [...capiUtxoCol, ...capiUtxoBtc],
        [{ address: address, amount: amountEwl }],
        feeSatPerKB,
        { walletBtc: walletBtc, walletCol: this }
      )

      hex = await ColoredHelper.signTx(hex, this, walletBtc)

      const txid = await this.sendRawTransaction(hex)
      logger.debug('tx %s broadcasted!', txid)

      return txid
    } catch (err) {
      if (err.message.includes('No output with the requested asset'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_ASSETS)

      if (err.message.includes('Not enough satoshi to cover transaction'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_BITCOIN)

      throw err
    }
  }

  public async sendMany(
    amounts: IColSendManyOutputs,
    confTarget?: number,
    estimateMode?: EstimateMode
  ): Promise<string> {
    const to: Array<{ address: string; amount: number; amountBtc?: number }> = []
    for (const [key, value] of Object.entries(amounts)) {
      if (!ColoredHelper.isValidAddress(key, this.coloredConfig)) throw new RainbohError(RainbohError.INVALID_ADDRESS)
      const amountColBtc = ColoredHelper.toAmountColBtc(value)
      const amountBtc = amountColBtc.amountBtc && ColoredHelper.toSat(amountColBtc.amountBtc)
      if (amountColBtc.amount < 1 / 10 ** this.coloredConfig.divisibility)
        throw new RainbohError(RainbohError.INVALID_AMOUNT)
      to.push({
        address: key,
        amount: ColoredHelper.toEwl(amountColBtc.amount, this.coloredConfig.divisibility),
        amountBtc: amountBtc
      })
    }

    const walletBtc = new WalletBtc(this.walletBtcConfig.name)

    const capiUtxoCol = await this.getCapiUtxos(1)
    const capiUtxoBtc = await walletBtc.getCapiUtxos(1)

    // estimateSmartFee responses with errors if bitcoind doesn't have enough information to calculate fees
    const feeEstimation = await this.estimateSmartFee(confTarget || 3, estimateMode)
    // if (feeEstimation.errors) throw new RainbohError(RainbohError.FEE_ESTIMATION_NOT_AVAILABLE)
    if (!feeEstimation.feerate) logger.warn('Fee estimation not available. Fee set to 1000 sat/KB')
    const feeRate = feeEstimation.feerate || 0.00001 // fallback to 1 sat/B

    const feeSatPerKB = ColoredHelper.toSat(feeRate)

    logger.debug('Choose fee: %s sat/KB', feeSatPerKB)

    try {
      let hex = await ColoredHelper.createTx([...capiUtxoCol, ...capiUtxoBtc], to, feeSatPerKB, {
        walletBtc: walletBtc,
        walletCol: this
      })

      hex = await ColoredHelper.signTx(hex, this, walletBtc)

      const txid = await this.sendRawTransaction(hex)
      logger.debug('tx %s broadcasted!', txid)

      return txid
    } catch (err) {
      if (err.message.includes('No output with the requested asset'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_ASSETS)

      if (err.message.includes('Not enough satoshi to cover transaction'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_BITCOIN)

      throw err
    }
  }

  public async getCapiUtxos(minConf: number = 0) {
    const listUnspent = await this.coreClient.listUnspent(minConf)

    const capiUtxos: CapiUtxo[] = []

    for (const unspent of listUnspent) {
      try {
        const capiTx = await this.capiDbProvider.getTransaction(unspent.txid)

        const utxoAssets = this.getUtxoAssetsInfo(capiTx, unspent.vout)

        capiUtxos.push({
          txid: unspent.txid,
          index: unspent.vout,
          value: ColoredHelper.toSat(unspent.amount),
          used: false,
          scriptPubKey: {
            addresses: [unspent.address],
            hex: unspent.scriptPubKey
          },
          assets: utxoAssets
        })
      } catch (err) {
        capiUtxos.push({
          txid: unspent.txid,
          index: unspent.vout,
          value: ColoredHelper.toSat(unspent.amount),
          used: false,
          scriptPubKey: {
            addresses: [unspent.address],
            hex: unspent.scriptPubKey
          },
          assets: []
        })
      }
    }

    return capiUtxos
  }

  public getColoredConfig() {
    return this.coloredConfig
  }

  public async opReturnLimit(amounts: number[]): Promise<number> {
    const ewlAmounts = amounts.map(amount => ColoredHelper.toEwl(amount, this.coloredConfig.divisibility))

    const coloredCoinsBuilder = new ColoredCoinsBuilder()
    return coloredCoinsBuilder.opReturnLimit({ amounts: ewlAmounts })
  }

  public async createPsbt(inputs: ICreatePsbtInputs, outputs: IColCreatePsbtOutputs): Promise<string> {
    const to: Array<{ address: string; amount: number; amountBtc?: number }> = []
    for (const output of outputs) {
      for (const [address, value] of Object.entries(output)) {
        if (!ColoredHelper.isValidAddress(address, this.coloredConfig))
          throw new RainbohError(RainbohError.INVALID_ADDRESS)
        const amounts = ColoredHelper.toAmountColBtc(value)

        const amountBtc = amounts.amountBtc && ColoredHelper.toSat(amounts.amountBtc)
        to.push({
          address: address,
          amount: ColoredHelper.toEwl(amounts.amount, this.coloredConfig.divisibility),
          amountBtc: amountBtc
        })
      }
    }

    const walletBtc = new WalletBtc(this.walletBtcConfig.name)

    // all the UTXOs are selected, even unconfirmed ones
    const capiUtxoCol = await this.getCapiUtxos()
    const capiUtxoBtc = await walletBtc.getCapiUtxos()

    const inputsSet = new Set(inputs.map(input => `${input.txid}:${input.vout}`))
    const utxos: CapiUtxo[] = []
    for (const utxo of [...capiUtxoCol, ...capiUtxoBtc]) {
      if (inputsSet.has(`${utxo.txid}:${utxo.index}`)) {
        utxos.push(utxo)
      }
    }

    if (utxos.length !== inputs.length) throw new RainbohError(RainbohError.TX_INPUTS_NOT_FOUND)

    try {
      const hex = await ColoredHelper.createRawColTx(utxos, to, {
        walletBtc: walletBtc,
        walletCol: this
      })
      return new Wallet().convertToPsbt(hex)
    } catch (err) {
      if (err.message.includes('No output with the requested asset'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_ASSETS)

      if (err.message.includes('Not enough satoshi to cover transaction'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_BITCOIN)

      throw err
    }
  }

  private getUtxoAssetInfo(capiTx: any, output: number, address?: string) {
    if (!capiTx) return undefined

    const capiVOut = address
      ? _.find(capiTx.vout, { n: output, scriptPubKey: { addresses: [address] } })
      : _.find(capiTx.vout, { n: output })
    if (!capiVOut) return undefined

    const assetInfo = _.find(capiVOut.assets, { assetId: this.coloredConfig.assetId })
    if (!assetInfo) return undefined

    return assetInfo
  }

  private getUtxoAssetsInfo(capiTx: any, output: number) {
    if (!capiTx) return undefined

    const capiVOut = _.find(capiTx.vout, { n: output })
    if (!capiVOut) return undefined

    return capiVOut.assets
  }
}

export default WalletCol
