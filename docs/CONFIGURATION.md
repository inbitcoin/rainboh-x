# Configuration

Several configuration options can be set in the `rainboh-config.json` file.
See [`rainboh-config.sample.json`](rainboh-config.sample.json) for a commented example.

Configuration file prototype for interface version **2.0**:

```typescript
{
  version: string                   // config version
  network: string                   // bitcoin network (possible values: 'mainnet', 'testnet', 'regtest')
  wallets: {                        // list of wallets to use
    bitcoin: Array<{                // list of bitcoin wallets to use
      name: string                  // wallet name
    }
    colored: Array<{                // list of colored wallets to use
      name: string                  // wallet name
      coin: string                  // wallet coin code. coin must be defined in coloredCoins
      btcWallet: string             // bitcoin wallet associated with the colored wallet.
                                    // must match a defined bitcoin wallet name
    }
    coloredOptions: {               // colored coins wallets options
      softMaxUtxos: number          // target UTXO set used in coin selection (min: 1, max: 1000)
    }
  }
  coloredCoins: Array<{             // supported colored coins
    coin: string                    // coin code
    name: string                    // coin full name
    addressPrefix: string           // prefix attached to colored addresses
    addressHrp: string              // prefix of bech32 colored addresses
    assetId: string                 // coin asset ID
    divisibility: number            // coin divisibility (min: 0, max:8)
  }>
  capi: {                           // Colored Coins instance parameters
    db: {                           // Colored Coins explorer DB parameters
      host: string                  // host
      port: string                  // port
      name: string                  // name
    }
  }
  bitcoind: {                       // bitcoind instance parameters
    version: string                 // version (0.18.x)
    host: string                    // host
    rpcPort: string                 // rpc port
    rpcUsername: string             // rpc username
    rpcPassword: string             // rpc password
    timeout?: number                // rpc connection timeout (ms) - optional (min: 1000, default: 30000)
  }
}
```

#### Note

- `version`: configuration version is of the format `Major.Minor`. A major bump signals backward-incompatible changes to the configuration interface, while a minor signals the compatible ones
- `wallets`: all the wallet names are defined under `wallets` and all the available coins are defined under `coloredCoins`
- `coin`: all `coin` values used in `wallets.colored` must have a corresponding coin defined in `coloredCoins` with a matching `coin` parameter
- `softMaxUtxos`: used by the [coin selection algorithm](/docs/SPEC.md#coin-selection) to keep the
  number of UTXOs around the requested number

On `yarn start` rainboh-x validates the configuration and raises an error if it detects something wrong.

## Environment variables

#### Node variables

- `PORT`: rainboh-x listening port (default `7872`)
- `NODE_ENV`: environment in which rainboh-x should run
  (possible values: `production`, `development`, `test`; default `development`)
- `LOG_LEVEL`: desired console log level
  (possible values: `error`, `warn`, `info`, `http`, `verbose`, `debug`, `silly`;
  default `info` when running in `production` environment, `debug` otherwise)
- `RPC_TIMEOUT`: rainboh-x rpc connection timeout, in seconds (default `120`)

#### Variables that override the configuration

- `CAPI_DB_HOST`
- `CAPI_DB_PORT`
- `CAPI_DB_NAME`
- `BITCOIND_VERSION`
- `BITCOIND_HOST`
- `BITCOIND_RPCPORT`
- `BITCOIND_RPCUSERNAME`
- `BITCOIND_RPCPASSWORD`
- `BITCOIND_TIMEOUT`
