Rainboh-x contributors, in chronological order of contribution:

- [Patrick Peterle](https://gitlab.com/patrix252) - patrix@inbitcoin.it
- [Zoe Faltibà](https://gitlab.com/zoedberg) - zoe@inbitcoin.it
- [Martino Salvetti](https://github.com/inaltoasinistra) - tldr@inbitcoin.it
