# Specification

- [Supported APIs](#supported-apis)
  - [Blockchain](#blockchain)
  - [Network](#network)
  - [Wallet](#wallet)
  - [Utility](#utility)
<br>

- [Software specification](#software-specification)
  - [Handling errors](#handling-errors)
  - [Coin selection](#coin-selection)

## Supported APIs

- **Blockchain**
  - [getblock](#getblock)
  - [getblockcount](#getblockcount)
  - [getblockhash](#getblockhash)
<br>

- **Network**
  - [getconnectioncount](#getconnectioncount)
  - [getpeerinfo](#getpeerinfo)
<br>

- **Wallet**
  - [createpsbt](#createpsbt)
  - [createwallet](#createwallet)
  - [decoderawtransaction](#decoderawtransaction)
  - [encryptwallet](#encryptwallet)
  - [finalizepsbt](#finalizepsbt)
  - [getbalance](#getbalance)
  - [getnewaddress](#getnewaddress)
  - [getrawtransaction](#getrawtransaction)
  - [gettransaction](#gettransaction)
  - [listtransactions](#listtransactions)
  - [listunspent](#listunspent)
  - [listwallets](#listwallets)
  - [loadwallet](#loadwallet)
  - [opreturnlimit](#opreturnlimit)
  - [sendmany](#sendmany)
  - [sendrawtransaction](#sendrawtransaction)
  - [sendtoaddress](#sendtoaddress)
  - [unloadwallet](#unloadwallet)
  - [walletlock](#walletlock)
  - [walletpassphrase](#walletpassphrase)
  - [walletpassphrasechange](#walletpassphrasechange)
  - [walletprocesspsbt](#walletprocesspsbt)
<br>

- **Utility**
  - [estimatesmartfee](#estimatesmartfee)
  - [getconfig](#getconfig)

### Blockchain

#### getblock

> getblock _"blockhash" ( verbosity )_

Gets info of block with hash `blockhash`, with requested `verbosity` (default `1`).

If verbosity is `0`, returns a string that is serialized, hex-encoded data for `blockhash`.
If verbosity is `1`, returns an Object with information about `blockhash`.
If verbosity is `2`, returns an Object with information about `blockhash` and information about
each transaction.

See [getblock on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/blockchain/getblock/)

#### getblockcount

> getblockcount

Returns the number of blocks in the longest chain.

See [getblockcount on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/blockchain/getblockcount/)

#### getblockhash

> getblockhash _height_

Returns hash of block in the longest chain at provided `height`.

See [getblockhash on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/blockchain/getblockhash/)

### Network

#### getconnectioncount

> getconnectioncount

Returns the number of open connections with other nodes.

See [getconnectioncount on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/network/getconnectioncount/)

#### getpeerinfo

> getpeerinfo

Returns data about each connected network node as a JSON array of objects.

See [getpeerinfo on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/network/getpeerinfo/)

### Wallet

All APIs in this section (except for `createwallet`, `loadwallet` and `unloadwallet`)
require specifying the wallet to be used.

#### createpsbt

> createpsbt _[{"txid":"hex","vout":n},...] [{"address":amount},{"address":{"amount":amount,"amountBtc":amount}},...]_

Creates a transaction in the Partially Signed Transaction format.

#### createwallet

> createwallet _"wallet_name"_

Creates and loads a new wallet with the provided `wallet_name`.

A wallet named `wallet_name` must already be defined in the configuration in the proper subsection
under `wallets`, otherwise an error will be returned.

- unsupported parameters: `disable_private_keys`, `blank`

See [createwallet on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/createwallet/)

#### decoderawtransaction

> decoderawtransaction _"hexstring"_ _( iswitness )_

Return a JSON object representing the serialized, hex-encoded transaction.

#### encryptwallet

> encryptwallet _"passphrase"_

Encrypts the wallet with `passphrase`.

See [encryptwallet on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/encryptwallet/)

#### finalizepsbt

> finalizepsbt _"psbt"_

Finalize the inputs of a PSBT. If the transaction is fully signed, it will produce a
network serialized transaction which can be broadcast with sendrawtransaction. Otherwise a PSBT will be
created which has the final_scriptSig and final_scriptWitness fields filled for inputs that are complete.
Implements the Finalizer and Extractor roles.

#### getbalance

> getbalance _( "dummy" minconf )_

Returns the total available balance.

By default, colored wallets show confirmed balances (`minconf` set to `1`), while bitcoin wallets
also show spendable balance (`minconf` set to `0`).
For colored wallets it returns the sum of spendable assets.
Parameter `dummy` must be set to `"*"`.

- unsupported parameters: `include_watchonly`

See [getbalance on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/getbalance/)

#### getnewaddress

> getnewaddress _"" address_type_

Returns a new address.

Address will be a bitcoin address when using a bitcoin wallet or a colored address (including
`addressPrefix` or `addressHrp`) when using a colored wallet.
Legacy and bech32 addresses can be generated.
The bitcoin wallets support three types of addresses: `legacy`, `p2sh-segwit` and `bech32`.
The colored wallets support two types of addresses: `legacy` and `bech32`.
The default address type of bitcoin wallets is `bech32`. The default value of colored wallets is `legacy`.

- unsupported parameters: `label`

See [getnewaddress on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/getnewaddress/)

#### gettransaction

> gettransaction _"txid"_

Get detailed information about an in-wallet transaction identified by `txid`.

For colored wallets `address` has `addressPrefix`, `balance` is colored, `balanceBtc` is the
bitcoin balance.
The Bitcoin Core wallet does not support transactions with inputs from different wallets,
so the `fee` fields of outgoing colored transactions are wrong.
The fields are not reliable on both colored and bitcoin wallets.

Moreover, the bitcoin transfers between bitcoin and colored wallets are considered as external wallet transfers. Almost every colored `sendtoaddress` moves bitcoins between the wallets. The `details` field could show a bitcoin `send` operation, but it is the bitcoin change output, not a real send.

- unsupported parameters: `include_watchonly`

See [gettransaction on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/gettransaction/)

#### getrawtransaction

> getrawtransaction _"txid"_ _( verbose "blockhash" )_

Return the raw transaction data.

By default this function only works for mempool transactions. When called with a blockhash argument, getrawtransaction will return the transaction if the specified block is available and the transaction is found in that block. When called without a blockhash argument, getrawtransaction will return the transaction if it is in the mempool, or if -txindex is enabled and the transaction is in a block in the blockchain.

#### listtransactions

> listtransactions _( "label (\*)" count skip )_

Returns up to `count` most recent transactions, skipping the first `skip` transactions.

The first param `label` is ignored and must be set to `"*"`.
For colored wallets the same differences as in `gettransaction` apply.

- unsupported parameters: `label`, `include_watchonly`

See [listtransactions on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/listtransactions/)

#### listunspent

> listunspent _( minconf maxconf )_

Returns an array of unspent transaction outputs with a number of confirmations between `minconf`
and `maxconf` (inclusive).

For colored wallets the same differences as in `gettransaction` apply.

- unsupported parameters: `address`, `include_unsafe`, `query_options`

See [listunspent on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/listunspent/)

#### listwallets

> listwallets

Returns a list of currently loaded wallets.

See [listwallets on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/listwallets/)

#### loadwallet

> loadwallet _"filename"_

Loads a wallet from `filename`, which can be a directory or a wallet file name.

The wallet has to be present in the configuration in the proper subsection
under `wallets`, otherwise an error will be returned.

See [loadwallet on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/loadwallet/)

#### sendmany

> sendmany _"" {"address":amount} "" "" "" "" conf_target estimate_mode_

Send to multiple `address`.

Address must not include the protocol prefix `bitcoin:`.
Both wallets, colored and bitcoin, need to be unlocked (if encrypted) and loaded.
For colored wallets fees are calculated using Bitcoin Core's `estimatesmartfee`
using `num_blocks=3`.

- estimate_mode values: UNSET, ECONOMICAL, CONSERVATIVE
- unsupported parameters: `minconf`, `comment`, `subtractfeefrom`, `replaceable`

See [sendmany on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/sendmany/)

#### sendrawtransaction

> sendrawtransaction _"hexstring"_

Submit a raw transaction (serialized, hex-encoded) to local node and network.

Note that the transaction will be sent unconditionally to all peers, so using this
for manual rebroadcast may degrade privacy by leaking the transaction's origin, as
nodes will normally not rebroadcast non-wallet transactions already in their mempool.


#### sendtoaddress

> sendtoaddress _"address" amount ( dummy dummy dummy dummy conf_target )_

Sends an `amount` to a given `address`.

Address must not include the protocol prefix `bitcoin:`.
Both wallets, colored and bitcoin, need to be unlocked (if encrypted) and loaded.
For colored wallets fees are calculated using Bitcoin Core's `estimatesmartfee`
using `num_blocks=3`.
The parameter conf_target modifies `num_blocks`.
conf_target is supported only with colored transactions.

- unsupported parameters: `comment`, `comment_to`, `subtractfeefromamount`, `replaceable`,
  `estimate_mode`

See [sendtoaddress on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/sendtoaddress/)

#### unloadwallet

> unloadwallet _"wallet_name"_

Unloads the wallet specified by `wallet_name`.

A wallet named `wallet_name` must alredy be defined in the configuration in the proper subsection
under `wallets`, otherwise an error will be returned.

See [unloadwallet on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/unloadwallet/)

#### walletlock

> walletlock

Removes the wallet encryption key from Bitcoin Core memory, locking the wallet.

After calling this method, you will need to call `walletpassphrase` again
before being able to call any methods requiring the wallet to be unlocked.

See [walletlock on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/walletlock/)

#### walletpassphrase

> walletpassphrase _"passphrase" timeout_

Stores the wallet decryption key in Bitcoin Core memory for `timeout` seconds.

This is needed prior to performing transactions related to private keys (e.g. `sendtoaddress`).

See [walletpassphrase on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/walletpassphrase/)

#### walletpassphrasechange

> walletpassphrasechange _"oldpassphrase" "newpassphrase"_

Changes the wallet passphrase from `oldpassphrase` to `newpassphrase`.

See [walletpassphrasechange on Bitcoin Core](https://bitcoincore.org/en/doc/0.18.0/rpc/wallet/walletpassphrasechange/)

#### walletprocesspsbt

> walletprocesspsbt _"psbt"_

Update a PSBT with input information from our wallet and then sign inputs
that we can sign for.


### Utility

#### estimatesmartfee

> estimatesmartfee _conf_target_

Estimates the approximate fee per kilobyte needed for a transaction to begin
confirmation within conf_target blocks if possible and return the number of blocks
for which the estimate is valid. Uses virtual transaction size as defined
in BIP 141 (witness data is discounted).


#### getconfig

Returns the current running configuration.
Only non sensitive information is returned, unless test mode (`NODE_ENV = test` ) is enabled.

#### opreturnlimit

> opreturnlimit _[amount0, amount1, …, amountM-1]_

Colored Coins limits the number of colored outputs of transactions.
This API computes how many outputs can be successfully sent.

The input is a list of amounts; the output is a number N, it means that a sendmany can be created with the amounts from 0 to N-1 (`amounts[0:N]`).

## Software specification

### Handling errors

Example output in case of error:

```json
{
  "error": {
    "code": 217,
    "name": "RainbohError",
    "message": "The wallet requested is not in the configuration"
  },
  "id": "curltest"
}
```

The list of all possible rainboh-x errors is available in
[`src/error/rainbohError.ts`](/src/error/rainbohError.ts).

### Coin selection

#### Colored wallet

The coin selection algorithm for colored UTXOs sequentally applies the following rules:

- it selects one UTXO with the exact requsted amount
- if not possible, it looks for one UTXO with an amount greater than the requested one
- if not possible, it selects UTXOs from the smallest to the biggest, until the selected UTXOs
  reach or exceeds the requested amount

After these rules, the configuration parameter `softMaxUtxos` is considered.
If the wallet has more UTXOs than the parameter, other UTXOs are added to the inputs.
This behaviour consolidates the UTXOs of the wallet.

The colored UTXOs coin selection is then over. If the selected UTXOs can pay for the mining fees of
the transaction, the coin selection algorithm is over too.

#### Bitcoin wallet

The bitcoin wallet is used only to pay mining fees.

If bitcoin UTXOs are needed, they are added one by one until the sum is enough to pay the mining
fees.

The bitcoin UTXOs are obtained with a `listunspent` RPC call to Bitcoin Core and are processed
in the same order they are received.
